clc;
clear vars;

%% Load data
load("t29.mat")
T = length(y)*dt-dt;
t = (0:dt:T)';

%% SISO identification
fu = fft(u);
fz = fft(z);
fy = fft(y);
Suu = conj(fu).*fu;
Suy = conj(fu).*fy;
Szz = conj(fz).*fz;

span = 50;
Suu = smooth(Suu, span);
Suy = smooth(Suy, span);
reduction = 5;
Suu = decimate(Suu,reduction);
Suy = decimate(Suy,reduction);
G_iw = Suy./Suu;

%% Nyquist plot
half = round(length(G_iw)/2);
nyquist_draw = G_iw(1:half);

figure;
subplot(2,1,1);
plot(nyquist_draw); grid on;
xlabel('Real');
ylabel('Imaginary');
subplot(2,1,2);
plot(real(nyquist_draw(1:1000))); hold on;
plot(-imag(nyquist_draw(1:1000))); grid on;
xlabel('Sample');
legend('Real', '-Imaginary');

%% G1 Nyquist identification
G1_SISO = NyqIdent(84.4*reduction, 92*reduction, T)

%% Compare
figure;
t = t(1:length(ys));
plot(t, ys, 'r'); hold on;
step(G1_SISO, t, 'b--'); grid on;
title('Identification SISO step response');
legend('Original', 'Identification');

figure;
plot(Szz, 'b'); hold on;
plot(Suu, 'r');