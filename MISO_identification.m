clc;
clear vars;

%% Load data
load("t29.mat")
T = length(y)*dt-dt;
t = (0:dt:T)';

%% MIsO identification
fu = fft(u);
fz = fft(z);
fy = fft(y);
Suu = fu.*conj(fu);
Szz = fz.*conj(fz);
Suy = conj(fu).*fy;
Szy = conj(fz).*fy;
Suz = conj(fu).*fz;
Szu = conj(fz).*fu;

span = 300;
Suu = smooth(Suu,span);
Szz = smooth(Szz,span);
Suy = smooth(Suy,span);
Szy = smooth(Szy,span);
Suz = smooth(Suz,span);
Szu = smooth(Szu,span);

reduction = 5;
Suu = decimate(Suu,reduction);
Szz = decimate(Szz,reduction);
Suy = decimate(Suy,reduction);
Szy = decimate(Szy,reduction);
Suz = decimate(Suz,reduction);
Szu = decimate(Szu,reduction);

G1 = (Suy.*Szz-Szy.*Suz)./(Suu.*Szz-Suz.*Szu);
G2 = (Suu.*Szy-Szu.*Suy)./(Suu.*Szz-Suz.*Szu);

samples = length(G1);
G1 = G1(1:samples/2);
G2 = G2(1:samples/2);

%% Nyquist plot
figure
subplot(2,1,1);
plot(G1); grid on;
xlabel('Real');
ylabel('Imaginary');
subplot(2,1,2);
plot(real(G1(1:1000))); hold on;
plot(-imag(G1(1:1000))); grid on;
xlabel('Sample');
legend('Re', 'Im');
title('Nyquist G1')

figure
subplot(2,1,1);
plot(G2); grid on;
xlabel('Real');
ylabel('Imaginary');
subplot(2,1,2);
plot(real(G2(1:1000))); hold on;
plot(-imag(G2(1:1000))); grid on;
xlabel('Sample');
legend('Re', 'Im');
title('Nyquist G2')

%% G1 Nyquist Identification
G1_MISO = NyqIdent(84*reduction, 91.6*reduction, T)

%% G2 Nyquist Identification
G2o_MISO = NyqIdent(73.1*reduction, 106.1*reduction, T)

To = 1/(2*pi*73.1*reduction/T);
G2i_MISO = tf(1/To, [1 1/To])

%% Compare
y1 = lsim(G1_MISO,u,t);
y2i = lsim(G2i_MISO,z,t);
y2o = lsim(G2o_MISO,z,t);
result_i = y1 + y2i;
result_o = y1 + y2o;
error_o = y-result_o;
error_i = y-result_i;
var_o = var(error_o)
var_i = var(error_i)

figure;
subplot(2,1,1);
plot(t, y); hold on;
plot(t, result_o); grid on;
ylabel('Amplitude');
xlabel('Time [s]');
title('G1+G2o output');
legend('Original', 'Simulation');

subplot(2,1,2);
plot(t, y); hold on;
plot(t, result_i); grid on;
ylabel('Amplitude');
xlabel('Time [s]');
title('G1+G2i output');
legend('Original', 'Simulation');