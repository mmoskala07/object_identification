% Nyquist Identification
% np_45 - sample number where arg[G(iW)] = -45 deg
% np_90 - sample number where arg[G(iW)] = -90 deg
% T     - duration, last value of time array
function [G] = NyqIdent(np_45, np_90, T)
    W1 = 2*pi*np_45/T;
    W2 = 2*pi*np_90/T;

    % Parameters of oscillating object
    Wo = W2
    Zeta = 0.5*(W2/W1)*(1-(W1/W2)^2)

    % Object transmittance
    G = tf(Wo^2, [1 2*Wo*Zeta Wo^2]);
end