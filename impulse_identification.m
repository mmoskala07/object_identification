clc;
clear vars;

%% Load data
load("t29.mat")
T = length(ys)*dt-dt;
t = (0:dt:T)';

%% Step response identification
d_ys = diff(ys);
d_ys = [d_ys' zeros(1, 9001)]';
fys = fft(d_ys);
half = round(length(fys)/2);
nyquist_draw = fys(1:half);

%% Nyquist plot
figure;
subplot(2,1,1);
plot(nyquist_draw); grid on;
xlabel('Real');
ylabel('Imaginary');
subplot(2,1,2);
plot(real(nyquist_draw(1:1000))); hold on;
plot(-imag(nyquist_draw(1:1000))); grid on;
xlabel('Sample');
legend('Real', '-Imaginary');

%% G1 Nyquist identification
T = length(d_ys)*dt-dt;
G_impulse = NyqIdent(204, 218.7, T) % Fill np_45 and np_90

%% Compare
figure;
plot(t, ys, 'r'); hold on;
step(G_impulse, t, 'b--'); grid on;
title('Identification impulse response');
legend('Identification', 'Original');