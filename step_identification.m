clc; 
clear vars;

%% Load data
load("t29.mat")
T = length(ys)*dt-dt;
t = (0:dt:T)';

%% Step response identification
delta_ym = max(ys) - 1;
period_damped = 0.0046297; % measure the period of damped oscillations !
Zeta = -log(delta_ym) / sqrt(log(delta_ym)^2 + pi^2)
Wo = 2*pi / (period_damped*sqrt(1-Zeta^2))
G1_step = tf(Wo.^2, [1 2*Zeta*Wo Wo.^2])

%% Compare
figure;
plot(t, ys, 'r'); hold on;
step(G1_step, t, 'b--'); grid on;
title('Identification step response');
legend('Original', 'Identification');