clc;
clear vars;
load("t29.mat");

%% Set model
is_miso = true;
model = oe121_miso;

%% Solve for Gu
[L_Gu, M_Gz] = th2tf(model(1),1);
Gu_d = tf(L_Gu,M_Gz,dt);
Gu_c = d2c(Gu_d);

[L_Gu_c, M_Gu_c] = tfdata(Gu_c, 'v');

M2 = M_Gu_c(end-1);
M3 = M_Gu_c(end);

syms Lu Cu
Ru = 120;
eqns = [1/(Lu*Cu)==M3, Ru/Lu==M2];
vars = [Lu Cu];
[Lu, Cu] = solve(eqns, vars);
Cu = vpa(Cu) * 10^6; % [microF];
Lu = vpa(Lu); % [H];

%% Solve for Gz
Cz = NaN;
Gz_c = NaN;
if is_miso
    % solve for Gz
    [L_Gz, M_Gz] = th2tf(model(2),1);
    Gz_d = tf(L_Gz,M_Gz,dt);
    Gz_c = d2c(Gz_d);

    [L_Gz_c, M_Gz_c] = tfdata(Gz_c, 'v');

    L0 = L_Gz_c(end);
    Rz = 1100;
    Cz = 1/(Rz*L0) * 10^6; % [microF]
end

%% Show data
Gu_c % = tf(1.852*10^6, [1 200.2 1.852*10^6])
Cu;
Lu;

Gz_c
Cz;

%% Compare
% Gu+Gz Gauss noise response
if is_miso
    T = length(u)*dt-dt;
    t = (0:dt:T)';
    y1 = lsim(Gu_c,u,t);
    y2 = lsim(Gz_c,z,t);
    result = y1 + y2;
    error = y - result;
    
    figure;
    subplot(2,1,1);
    plot(t, y, 'r'); hold on;
    plot(t, result); grid on;
    ylabel('Amplitude');
    xlabel('Time [s]');
    title('Gu+Gz output');
    legend('Original', 'Simulation');
    subplot(2,1,2);
    plot(t, error); grid on;
    ylabel('Error');
    xlabel('Time [s]');
    title('Error plot');
end

% Gu step response
T = length(ys)*dt-dt;
t = (0:dt:T)';
result = lsim(Gu_c, ones(length(t),1),t);
error = ys - result;

figure;
subplot(2,1,1);
plot(t, ys, 'r'); hold on;
plot(t, result, 'b--'); grid on;
ylabel('Amplitude');
xlabel('Time [s]');
title('Gu step response');
legend('Original', 'Simulation');
subplot(2,1,2);
plot(t, error); grid on;
ylabel('Error');
xlabel('Time [s]');
title('Error plot');
